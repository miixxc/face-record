import {PermissionsAndroid} from 'react-native';
import {PERMISSION_TIPS} from './constant';

export default async function requestPermission() {
  let result = false;
  try {
    const g1 = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PERMISSION_TIPS.CAMERA,
    );
    const g2 = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      PERMISSION_TIPS.WRITE_EXTERNAL_STORAGE,
    );
    console.log('abc', g1, g2)
    result =
      g1 === PermissionsAndroid.RESULTS.GRANTED &&
      g2 === PermissionsAndroid.RESULTS.GRANTED;
  } catch (err) {
    console.warn(err);
  }
  return result;
}
