import {RNCamera} from 'react-native-camera';

export const RECORDING_DURATION = 3000;
export const SINGLE_OUTPUT_DURATION = '00:00:01';
export const SINGLE_CROPPING_START = '00:00:01';

export const CAMERA_SETTING = {
  flash: 'off',
  zoom: 0,
  autoFocus: 'on',
  captureAudio: false,
  autoFocusPoint: {
    normalized: {x: 0.5, y: 0.5}, // normalized values required for autoFocusPointOfInterest
  },
  depth: 0,
  type: 'front',
  whiteBalance: 'auto',
  ratio: '4:3',
  recordOptions: {
    // mute: true,
    maxDuration: 5,
    quality: RNCamera.Constants.VideoQuality['4:3'],
  },
};

export const PERMISSION_TIPS = {
  CAMERA: {
    title: '使用相机的权限',
    message: '我们需要您的许可才能使用您的相机',
    buttonPositive: '确定',
    buttonNegative: '取消',
  },
  WRITE_EXTERNAL_STORAGE: {
    title: '使用存储权限',
    message: '我们需要您的许可才能使用存储权限',
    buttonPositive: '确定',
    buttonNegative: '取消',
  },
};

export const STEP_NAMES = ['眨眼', '点头', '张嘴'];

export const ES_PATH = '/DCIM/FaceRecord' 
