/* eslint-disable no-console */
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import RNFS, {CachesDirectoryPath} from 'react-native-fs';
import {RNFFmpeg} from 'react-native-ffmpeg';
import dayjs from 'dayjs'
import {
  SINGLE_CROPPING_START,
  SINGLE_OUTPUT_DURATION,
  ES_PATH,
} from '../utils/constant';

const screenWidth = Math.round(Dimensions.get('window').width);
const centerBoxSize = Math.round(screenWidth * 0.6);
const centerBoxPadding = Math.round((screenWidth - centerBoxSize) * 0.5);

export const mergeVideo = async () => {
  const esPath = ES_PATH;
  const outputPath = RNFS.ExternalStorageDirectoryPath + esPath;
  const outputFile = dayjs().format('YYYYMMDDHHmmss') + '.mp4';
  const vListFile = 'vList.txt';
  const vList = Array.from(new Array(3), (v, k) => k + 1).reduce(
    (sum, item) =>
      (sum += `file '${RNFS.CachesDirectoryPath}/Camera/t${item}.mp4'\n`),
    '',
  );
  let result = '';
  try {
    const isExistOutputPath = await RNFS.exists(outputPath);
    if (!isExistOutputPath) {
      await RNFS.mkdir(outputPath);
    }
    await RNFS.writeFile(
      RNFS.DocumentDirectoryPath + '/' + vListFile,
      vList,
      'utf8',
    );
    await RNFFmpeg.execute(
      `-y -f concat -safe 0 -i ${RNFS.DocumentDirectoryPath +
        '/' +
        vListFile} -c copy ${outputPath + '/' + outputFile}`,
    );
    await RNFS.unlink(`${RNFS.CachesDirectoryPath}/Camera/`);
    result = esPath + '/' + outputFile;
  } catch (error) {
    console.error(error);
  }
  return result;
};

export default class CameraScreen extends React.Component {
  state = {
    isRecording: false,
    showDebugButton: false,
  };

  takePicture = async function() {
    console.log('takePicture');
    let uri = '';
    if (this.camera) {
      const data = await this.camera.takePictureAsync({});
      uri = data.uri;
      console.warn('takePicture ', data.uri);
    }
    return uri;
  };

  takeVideo = async function(num = 1) {
    console.log('takeVideo');
    let uri = '';
    if (this.camera) {
      try {
        const promise = this.camera.recordAsync({
          quality: RNCamera.Constants.VideoQuality['4:3'],
        });

        if (promise) {
          this.setState({isRecording: true});
          const data = await promise;
          this.setState({isRecording: false});
          if (data && data.uri) {
            const inputFile = data.uri.replace('file://', '');
            const outputFile = RNFS.CachesDirectoryPath + `/Camera/t${num}.mp4`;
            // const outputFile2 = RNFS.ExternalStorageDirectoryPath + `/Download/t${num}.mp4`
            await RNFFmpeg.execute(
              `-y -ss ${SINGLE_CROPPING_START} -i ${inputFile} -t ${SINGLE_OUTPUT_DURATION} ${outputFile}`,
            );
            // await RNFS.copyFile(
            //   outputFile,
            //   outputFile2,
            // );
            uri = data.uri;
          }
        }
      } catch (e) {
        console.error(e);
      }
    }
    return uri;
  };

  takeVideoDone() {
    if (this.camera) {
      this.camera.stopRecording();
    }
  }

  renderCamera() {
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          justifyContent: 'space-between',
        }}
        type="front"
        flashMode="off"
        autoFocus="on"
        captureAudio={false}
        autoFocusPointOfInterest={{x: 0.5, y: 0.5}}
        zoom={0}
        whiteBalance="auto"
        ratio="4:3"
        focusDepth={0}>
        <View style={styles.centerBox}>
          {/* <View style={[styles.autoFocusBox, drawFocusRingPosition]} /> */}
        </View>

        {this.state.showDebugButton && (
          <View style={{position: 'absolute', top: 12, left: 12}}>
            <TouchableOpacity
              style={[
                styles.flipButton,
                {backgroundColor: 'red', marginBottom: 12},
              ]}
              onPress={
                this.state.isRecording ? () => {} : () => this.takeVideo(1)
              }>
              {this.state.isRecording ? (
                <Text style={styles.flipText}> STOP </Text>
              ) : (
                <Text style={styles.flipText}> REC </Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.flipButton, {backgroundColor: 'green'}]}
              onPress={this.takePicture.bind(this)}>
              <Text style={styles.flipText}> SNAP </Text>
            </TouchableOpacity>
          </View>
        )}
      </RNCamera>
    );
  }

  render() {
    return <View style={[styles.container]}>{this.renderCamera()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 0.5,
    height: screenWidth,
    overflow: 'hidden',
    backgroundColor: '#000',
  },
  flipButton: {
    borderRadius: 8,
    borderColor: 'white',
    borderWidth: 1,
    padding: 5,
  },
  centerBox: {
    position: 'absolute',
    top: centerBoxPadding,
    left: centerBoxPadding,
    height: centerBoxSize,
    width: centerBoxSize,
    borderWidth: 2,
    borderColor: 'green',
    opacity: 0.5,
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  flipText: {
    color: 'white',
    fontSize: 15,
  },
  zoomText: {
    position: 'absolute',
    bottom: 70,
    zIndex: 2,
    left: 2,
  },
  picButton: {
    backgroundColor: 'darkseagreen',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: 2,
    height: 2,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
});
