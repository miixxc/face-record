/* eslint-disable no-console */
import React, {useState, useRef, useEffect} from 'react';
import {StatusBar, Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import CameraScreen, {mergeVideo} from './src/components/CameraScreen';
import Button from 'react-native-button';
import {RECORDING_DURATION, STEP_NAMES} from './src/utils/constant';
import requestPermission from './src/utils/requestPermission';

const styles = StyleSheet.create({
  controlBar: {
    flexDirection: 'column',
    paddingTop: 24,
    paddingBottom: 24,
  },
  noPermission: {
    flex: 1,
    backgroundColor: '#ccc',
    justifyContent: 'center',
  },
  noPermissionTips: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 12,
  },
  noPermissionTipsSub: {
    textAlign: 'center',
    fontSize: 14,
  },
  actionTips: {
    textAlign: 'center',
    fontSize: 18,
    marginBottom: 6,
  },
  actionTipsSub: {
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 40,
  },
  actionTipsEm: {
    color: 'green',
    fontWeight: 'bold',
    fontSize: 28,
  },
  actionButtonWrap: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  actionButtonContainer: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#007aff',
    borderRadius: 8,
  },
  actionButtonDisabled: {
    backgroundColor: '#f5a442',
  },
  actionButton: {
    color: '#fff',
    fontSize: 32,
  },
});

export default () => {
  const [step, setStep] = useState(0);
  const [progress, setProgress] = useState(false);
  const [outputInfo, setOutputInfo] = useState('');
  const cameraScreenRef = useRef(null);
  async function processVideo() {
    setProgress(true);
    const opi = await mergeVideo();
    setOutputInfo(opi);
    setProgress(false);
  }
  async function checkPermission() {
    const res = await requestPermission();
    if (res) {
      setStep(1);
    }
  }
  useEffect(() => {
    if (step === 0) {
      checkPermission();
    } else if (step === 4) {
      processVideo();
    } else {
      setOutputInfo('');
    }
  }, [step]);
  return (
    <>
      <StatusBar backgroundColor="black" />
      {step === 0 ? (
        <View style={styles.noPermission}>
          <Text style={styles.noPermissionTips}>系统提示</Text>
          <Text style={styles.noPermissionTipsSub}>
            等待用户对设备摄像头和文件存储授权
          </Text>
        </View>
      ) : (
        <CameraScreen ref={cameraScreenRef} />
      )}
      {step > 0 && step < 4 && (
        <View style={styles.controlBar}>
          <Text style={styles.actionTips}>
            即将录制&nbsp;&nbsp;
            <Text style={styles.actionTipsEm}>{STEP_NAMES[step - 1]}</Text>
            &nbsp;&nbsp;动作
          </Text>
          <Text style={styles.actionTipsSub}>步骤 {step}/3</Text>
          <View style={styles.actionButtonWrap}>
            <Button
              style={[styles.actionButton]}
              containerStyle={styles.actionButtonContainer}
              disabledContainerStyle={styles.actionButtonDisabled}
              disabled={progress}
              onPress={async () => {
                setProgress(true);
                const {current: camera} = cameraScreenRef;
                setTimeout(() => {
                  camera.takeVideoDone();
                }, RECORDING_DURATION);
                const res = await camera.takeVideo(step);
                const nextStep = step < 4 ? step + 1 : 0;
                setStep(nextStep);
                setProgress(false);
              }}>
              {progress ? '正在录制' : '开始'}
            </Button>
          </View>
        </View>
      )}
      {step === 4 && (
        <View style={styles.controlBar}>
          <Text style={styles.actionTips}>
            {progress ? '视频正在处理...' : '视频处理完成'}
          </Text>
          <Text style={styles.actionTipsSub}>{outputInfo || ' '}</Text>
          <View style={styles.actionButtonWrap}>
            <Button
              style={[styles.actionButton]}
              containerStyle={styles.actionButtonContainer}
              disabledContainerStyle={styles.actionButtonDisabled}
              disabled={progress}
              onPress={() => {
                setStep(1);
              }}>
              再录一次
            </Button>
          </View>
        </View>
      )}
    </>
  );
};
